package com.danny.cambodiatelecom.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;

import com.danny.cambodiatelecom.R;

public class CustomProgressDialog extends ProgressDialog {

    public CustomProgressDialog(Context context) {
        super(context);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.getWindow().setDimAmount(0);
        this.setCancelable(false);
    }

    @Override
    public void show() {
        super.show();
        try {
            setContentView(R.layout.custom_progress_dialog);
            getWindow().setGravity(Gravity.CENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
