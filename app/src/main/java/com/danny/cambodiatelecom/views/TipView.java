package com.danny.cambodiatelecom.views;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.danny.cambodiatelecom.R;

/**
 * Created by Danny on 3/16/2017.
 */

public class TipView extends FrameLayout {

    public TipView(@NonNull Context context) {
        super(context);
        initView();
    }

    public TipView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public TipView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView(){
        LayoutInflater.from(getContext()).inflate(R.layout.tip_view, this);
    }

    public void setImageTip(int imageTip) {
        ((ImageView) findViewById(R.id.call)).setImageResource(imageTip);
    }
}
