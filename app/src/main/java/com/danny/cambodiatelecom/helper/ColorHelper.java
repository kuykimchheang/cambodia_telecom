package com.danny.cambodiatelecom.helper;

import com.danny.cambodiatelecom.R;

/**
 * Created by Danny on 3/24/2017.
 */

public class ColorHelper {

    public static int getCallIcon(String color) {
        switch (color) {
            case "#F9A01B":
                return R.drawable.ic_call_cellcard;
            case "#1AAB4F":
                return R.drawable.ic_call_smart;
            case "#03928C":
                return R.drawable.ic_call_metfone;
            default:
                return R.drawable.ic_default;
        }
    }
}
