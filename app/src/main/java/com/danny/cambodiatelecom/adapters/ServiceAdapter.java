package com.danny.cambodiatelecom.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.activities.ServiceActivity;
import com.danny.cambodiatelecom.activities.ServiceListActivity;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

/**
 * Created by Danny on 3/10/2017.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceAdapterHolder> {

    private ArrayList<DataSnapshot> datas = new ArrayList<>();

    @Override
    public ServiceAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ServiceAdapterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ServiceAdapterHolder holder, int position) {
        holder.bind(datas.get(position));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void addAllItems(ArrayList<DataSnapshot> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        notifyDataSetChanged();
    }

    class ServiceAdapterHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ServiceAdapterHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv);
        }

        void bind(final DataSnapshot dataSnapshot) {
            final String name = dataSnapshot.child("tran_name").child("en").getValue(String.class);
            textView.setText(name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ServiceListActivity.launch(itemView.getContext(),
                            ((ServiceActivity) itemView.getContext()).getUrl() + "/" + dataSnapshot.getKey() + "/item", name);
                }
            });
        }
    }
}
