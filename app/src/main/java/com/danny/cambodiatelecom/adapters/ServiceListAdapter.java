package com.danny.cambodiatelecom.adapters;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.activities.ServiceListActivity;
import com.danny.cambodiatelecom.helper.ColorHelper;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

/**
 * Created by Danny on 3/10/2017.
 */

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.ServiceListAdapterHolder> {

    private ArrayList<DataSnapshot> dataSnapshots = new ArrayList<>();

    @Override
    public ServiceListAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ServiceListAdapterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.service_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ServiceListAdapterHolder holder, int position) {
        holder.bind(dataSnapshots.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSnapshots.size();
    }

    public void addAllItems(ArrayList<DataSnapshot> dataSnapshots) {
        this.dataSnapshots.clear();
        this.dataSnapshots.addAll(dataSnapshots);
        notifyDataSetChanged();
    }

    class ServiceListAdapterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        TextView code;
        ImageView call, info;
        DataSnapshot dataSnapshot;

        ServiceListAdapterHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            code = (TextView) itemView.findViewById(R.id.code);
            call = (ImageView) itemView.findViewById(R.id.call);
            info = (ImageView) itemView.findViewById(R.id.info);
            call.setOnClickListener(this);
            info.setOnClickListener(this);
        }

        void bind(final DataSnapshot dataSnapshot) {
            this.dataSnapshot = dataSnapshot;
            call.setImageResource(ColorHelper.getCallIcon(((ServiceListActivity) itemView.getContext()).getToolbarColor()));
            title.setText(dataSnapshot.child("name").child("en").getValue(String.class));
            code.setText(dataSnapshot.child("code").getValue(String.class));
        }

        @Override
        public void onClick(View v) {
            if (v == call) {
                if (dataSnapshot.child("is_input").getValue(Boolean.class) != null && dataSnapshot.child("is_input").getValue(Boolean.class))
                    showDialogInput();
                else
                    call(dataSnapshot.child("code").getValue(String.class));
            } else if (v == info) {
                new AlertDialog.Builder(itemView.getContext()).setMessage(dataSnapshot.child("info").child("en").getValue(String.class))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        }

        private void showDialogInput() {
            AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
            final EditText editText = new EditText(itemView.getContext());
            editText.setHint(R.string.enter_scratchcards_code);
            editText.setTextColor(Color.BLACK);
            editText.setTextSize(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.dialog_edittext_size));
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            FrameLayout frameLayout = new FrameLayout(itemView.getContext());
            frameLayout.addView(editText);
            frameLayout.setPadding(itemView.getContext().getResources().getDimensionPixelSize(R.dimen.padding_dialog_edittext), 0, itemView.getContext().getResources().getDimensionPixelSize(R.dimen.padding_dialog_edittext), 0);
            builder.setTitle(R.string.input_scratchcards_code);
            builder.setView(frameLayout);
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setPositiveButton(R.string.call, null);
            final AlertDialog alertDialog = builder.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            call(dataSnapshot.child("head_code").getValue(String.class) + editText.getText().toString() + "#");
                            alertDialog.dismiss();
                        }
                    });
                }
            });
            alertDialog.show();
        }

        private void call(String tel) {
            if (ContextCompat.checkSelfPermission(itemView.getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(itemView.getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                    itemView.getContext().startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Uri.encode(tel))));
            } else
                ActivityCompat.requestPermissions((ServiceListActivity) itemView.getContext(), new String[]{Manifest.permission.CALL_PHONE}, 111);
        }
    }
}
