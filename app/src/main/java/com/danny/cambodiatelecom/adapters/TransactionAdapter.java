package com.danny.cambodiatelecom.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.activities.ServiceActivity;
import com.danny.cambodiatelecom.activities.ServiceListActivity;
import com.danny.cambodiatelecom.activities.TransactionActivity;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

/**
 * Created by Danny on 3/10/2017.
 */

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.TransactionAdapterHolder> {

    private ArrayList<DataSnapshot> dataSnapshots = new ArrayList<>();

    @Override
    public TransactionAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TransactionAdapterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item, parent, false));
    }

    @Override
    public void onBindViewHolder(TransactionAdapterHolder holder, int position) {
        holder.bind(dataSnapshots.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSnapshots.size();
    }

    public void addAllItems(ArrayList<DataSnapshot> dataSnapshots) {
        this.dataSnapshots.clear();
        this.dataSnapshots.addAll(dataSnapshots);
        notifyDataSetChanged();
    }

    class TransactionAdapterHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView imageView;

        TransactionAdapterHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            imageView = (ImageView) itemView.findViewById(R.id.img);
        }

        void bind(final DataSnapshot dataSnapshot) {
            final String name = dataSnapshot.child("tran_name").child("en").getValue(String.class);
            title.setText(name);
            title.setTextColor(Color.parseColor(((TransactionActivity) itemView.getContext()).getToolbarColor()));
            Glide.with(itemView.getContext()).load(dataSnapshot.child("image").getValue(String.class))
                    .placeholder(R.drawable.no_image).into(imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dataSnapshot.child("sub_item").getValue(Boolean.class))
                        ServiceActivity.launch(itemView.getContext(),
                                ((TransactionActivity) itemView.getContext()).getUrl() + "/" + dataSnapshot.getKey() + "/item",
                                ((TransactionActivity) itemView.getContext()).getToolbarColor(), name);
                    else
                        ServiceListActivity.launch(itemView.getContext(),
                                ((TransactionActivity) itemView.getContext()).getUrl() + "/" + dataSnapshot.getKey() + "/item", name);
                }
            });
        }
    }
}
