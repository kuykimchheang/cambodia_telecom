package com.danny.cambodiatelecom.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.activities.TransactionActivity;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

/**
 * Created by Danny on 3/10/2017.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainAdapterHolder> {

    private ArrayList<DataSnapshot> datas = new ArrayList<>();

    @Override
    public MainAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainAdapterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.main_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MainAdapterHolder holder, int position) {
        holder.bind(datas.get(position));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void addAllItems(ArrayList<DataSnapshot> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        notifyDataSetChanged();
    }

    class MainAdapterHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        LinearLayout layout;

        public MainAdapterHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img);
            textView = (TextView) itemView.findViewById(R.id.tv);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
        }

        void bind(final DataSnapshot dataSnapshot) {
            textView.setText(dataSnapshot.child("name").getValue(String.class));
            Glide.with(itemView.getContext()).load(dataSnapshot.child("image").getValue(String.class))
                    .placeholder(R.drawable.no_image).into(imageView);
            layout.setBackgroundColor(Color.parseColor(dataSnapshot.child("color").getValue(String.class)));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TransactionActivity.launch(itemView.getContext(), dataSnapshot.child("key").getValue(String.class),
                            dataSnapshot.child("color").getValue(String.class), dataSnapshot.child("name").getValue(String.class));
                    ;
                }
            });
        }
    }
}
