package com.danny.cambodiatelecom.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.core.deps.guava.collect.Lists;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.adapters.ServiceListAdapter;
import com.danny.cambodiatelecom.helper.ColorHelper;
import com.danny.cambodiatelecom.views.TipView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

/**
 * Created by Danny on 3/10/2017.
 */

public class ServiceListActivity extends BaseActivity {
    private ServiceListAdapter adapter;
    private DatabaseReference databaseReference;

    public static void launch(Context context, String url, String title) {
        Intent intent = new Intent(context, ServiceListActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setUpToolbar(toolbar);
        toolbar.setTitle(getIntent().getStringExtra("title"));
        SharedPreferences sharedPreferences = getSharedPreferences("APP", MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        final TipView tipView = (TipView) findViewById(R.id.tip_view);
        tipView.setImageTip(ColorHelper.getCallIcon(getToolbarColor()));
        if (sharedPreferences.getBoolean("SHOW_TIP", true)) {
            tipView.setVisibility(View.VISIBLE);
        } else {
            tipView.setVisibility(View.GONE);
        }
        findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipView.setVisibility(View.GONE);
                editor.putBoolean("SHOW_TIP", false);
                editor.apply();
            }
        });
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_service);
        recyclerView.setLayoutManager(new LinearLayoutManager(ServiceListActivity.this));
        adapter = new ServiceListAdapter();
        recyclerView.setAdapter(adapter);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(getIntent().getStringExtra("url"));
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("EDDDDDD", dataSnapshot.toString());
                adapter.addAllItems(Lists.newArrayList(dataSnapshot.getChildren()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("EDDDDDD", databaseError.toString());
            }
        });
    }

    public void add(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_service_item, null);
        builder.setView(dialogView);
        builder.setTitle("New Service Item");
        final EditText code = (EditText) dialogView.findViewById(R.id.code);
        final EditText name_en = (EditText) dialogView.findViewById(R.id.name_en);
        final EditText name_km = (EditText) dialogView.findViewById(R.id.name_km);
        final EditText info_en = (EditText) dialogView.findViewById(R.id.info_en);
        final EditText info_km = (EditText) dialogView.findViewById(R.id.info_km);

        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        HashMap<String, Object> result = new HashMap<>();
                        HashMap<String, Object> info = new HashMap<>();
                        HashMap<String, Object> name = new HashMap<>();
                        info.put("en", info_en.getText().toString());
                        info.put("km", info_km.getText().toString());
                        name.put("en", name_en.getText().toString());
                        name.put("km", name_km.getText().toString());
                        result.put("code", code.getText().toString());
                        result.put("info", info);
                        result.put("name", name);

                        databaseReference.push().child(getIntent().getStringExtra("url"));
                        databaseReference.push().setValue(result).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ServiceListActivity.this, "Success add service.", Toast.LENGTH_SHORT).show();
                            }
                        });
                        alertDialog.dismiss();
                    }
                });
            }
        });
        alertDialog.show();
    }
}
