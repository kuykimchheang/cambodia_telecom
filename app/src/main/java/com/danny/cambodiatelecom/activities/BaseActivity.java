package com.danny.cambodiatelecom.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.danny.cambodiatelecom.views.CustomProgressDialog;

/**
 * Created by Danny on 3/10/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public static String toolbarColor = "#001ade";
    private Toolbar toolbar;
    protected CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new CustomProgressDialog(this);
        Log.d("ActivityLifeCycle", "onCreate " + getClass().getSimpleName());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("ActivityLifeCycle", "onRestart " + getClass().getSimpleName());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("ActivityLifeCycle", "onPause " + getClass().getSimpleName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ActivityLifeCycle", "onResume " + getClass().getSimpleName());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("ActivityLifeCycle", "onStart " + getClass().getSimpleName());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("ActivityLifeCycle", "onStop " + getClass().getSimpleName());
    }

    public void setUpToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        this.toolbar = toolbar;
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        toolbar.setTitleTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.parseColor("#15000000"));
        }
        toolbar.setBackgroundColor(Color.parseColor(getToolbarColor()));
        setSupportActionBar(toolbar);
    }

    public void setToolbarTitle(String toolbarTitle) {
        toolbar.setTitle(toolbarTitle);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public String getToolbarColor() {
        return toolbarColor;
    }

    public void setToolbarColor(String toolbarColor) {
        BaseActivity.toolbarColor = toolbarColor;
    }
}


