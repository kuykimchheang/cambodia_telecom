package com.danny.cambodiatelecom.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.test.espresso.core.deps.guava.collect.Lists;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.adapters.MainAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class MainActivity extends BaseActivity {

    private MainAdapter mainAdapter;
    private String color;
    private String image;
    private String name;
    private String key;
    private String status;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressDialog.show();
        setUpToolbar((Toolbar) findViewById(R.id.toolbar));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
        mainAdapter = new MainAdapter();
        recyclerView.setAdapter(mainAdapter);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        databaseReference = database.getReference("categories");
        databaseReference.orderByChild("status").equalTo("active").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                Log.d("EDDDDDD", dataSnapshot.toString());
                mainAdapter.addAllItems(Lists.newArrayList(dataSnapshot.getChildren()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("EDDDDDD", databaseError.toString());
                progressDialog.dismiss();
            }
        });

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                final View dialogView = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_add_brand, null);
                builder.setView(dialogView);
                builder.setTitle("New Brand");
                final EditText title = (EditText) dialogView.findViewById(R.id.title);
                final EditText color = (EditText) dialogView.findViewById(R.id.color);
                final EditText image = (EditText) dialogView.findViewById(R.id.image);
                final EditText key = (EditText) dialogView.findViewById(R.id.key);
                final EditText status = (EditText) dialogView.findViewById(R.id.status);
                image.setText("https://firebasestorage.googleapis.com/v0/b/cambodiatelecom-9c91d.appspot.com/o/brand_image%2Fic_default.PNG?alt=media&token=ee3aa522-3253-425e-9cac-7c3060de8555");

                builder.setPositiveButton("OK", null);
                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final AlertDialog alertDialog = builder.create();
                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        b.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                setName(title.getText().toString());
                                setColor(color.getText().toString());
                                setKey(key.getText().toString());
                                setImage(image.getText().toString());
                                setStatus(status.getText().toString());
                                addBrand();
                                alertDialog.dismiss();
                            }
                        });
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void addBrand() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("color", getColor());
        result.put("image", getImage());
        result.put("name", getName());
        result.put("key", getKey());
        result.put("status", getStatus());

        databaseReference.push().child("categories");
        databaseReference.push().setValue(result).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(MainActivity.this, "Success add guest.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
