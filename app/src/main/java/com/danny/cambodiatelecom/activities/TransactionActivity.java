package com.danny.cambodiatelecom.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.core.deps.guava.collect.Lists;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.adapters.TransactionAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Danny on 3/10/2017.
 */

public class TransactionActivity extends BaseActivity {

    private TransactionAdapter adapter;

    public static void launch(Context context, String key, String color, String title) {
        Intent intent = new Intent(context, TransactionActivity.class);
        intent.putExtra("key", key);
        intent.putExtra("color", color);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        progressDialog.show();
        setToolbarColor(getIntent().getStringExtra("color"));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setUpToolbar(toolbar);
        toolbar.setTitle(getIntent().getStringExtra("title"));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_brand_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(TransactionActivity.this));
        adapter = new TransactionAdapter();
        recyclerView.setAdapter(adapter);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(getUrl());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("EDDDDDD", dataSnapshot.toString());
                progressDialog.dismiss();
                adapter.addAllItems(Lists.newArrayList(dataSnapshot.getChildren()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
                Log.d("EDDDDDD", databaseError.toString());
            }
        });
    }

    public String getUrl() {
        return "brand/" + getIntent().getStringExtra("key");
    }
}
