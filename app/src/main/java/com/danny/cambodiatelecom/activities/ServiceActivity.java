package com.danny.cambodiatelecom.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.test.espresso.core.deps.guava.collect.Lists;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.danny.cambodiatelecom.R;
import com.danny.cambodiatelecom.adapters.ServiceAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

/**
 * Created by Danny on 3/14/2017.
 */

public class ServiceActivity extends BaseActivity {

    private ServiceAdapter adapter;
    private DatabaseReference databaseReference;

    public static void launch(Context context, String url, String color, String title) {
        Intent intent = new Intent(context, ServiceActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("color", color);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setUpToolbar(toolbar);
        toolbar.setTitle(getIntent().getStringExtra("title"));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_brand);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ServiceAdapter();
        recyclerView.setAdapter(adapter);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(getUrl());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("EDDDDDD", dataSnapshot.toString());
                adapter.addAllItems(Lists.newArrayList(dataSnapshot.getChildren()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("EDDDDDD", databaseError.toString());
            }
        });
    }

    public String getUrl() {
        return getIntent().getStringExtra("url");
    }

    public void add(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_service, null);
        builder.setView(dialogView);
        builder.setTitle("New Service");
        final EditText name_en = (EditText) dialogView.findViewById(R.id.name_en);
        final EditText name_km = (EditText) dialogView.findViewById(R.id.name_km);

        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        HashMap<String, Object> result = new HashMap<>();
                        HashMap<String, Object> name = new HashMap<>();
                        name.put("en", name_en.getText().toString());
                        name.put("km", name_km.getText().toString());
                        result.put("item", "");
                        result.put("tran_name", name);
                        result.put("type_cd", "list");

                        databaseReference.push().child(getUrl());
                        databaseReference.push().setValue(result).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ServiceActivity.this, "Success add service.", Toast.LENGTH_SHORT).show();
                            }
                        });
                        alertDialog.dismiss();
                    }
                });
            }
        });
        alertDialog.show();
    }
}
